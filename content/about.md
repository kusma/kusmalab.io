---
title: About me
menus:
  main
---
{{< me >}}

I'm a graphics programmer, specializing in low-level OpenGL / DirectX, and
other GPU-related development.

### Professional

I currently work at [Collabora](https://www.collabora.com), as a Principal
Engineer in the graphics team. Most of my work currently evolve around
[Mesa](https://www.mesa3d.org).

Previously, I worked at [Fusetools](https://fusetools.com), leading the
development of the compiler for the
[Uno](https://fuseopen.com/docs/uno/uno-lang)-language (a dialect of C#),
as well as [fuselibs](https://github.com/fuse-open/fuselibs), which is the
core of the UI engine.

I've also previously worked at Hue AS (now [Bluware](https://bluware.com/)),
building big-scale data visualization software for the Oil and Gas industry.

Before that, I was one of the first employees at Falanx Microsystems (later
aquired by ARM), where I worked on the
[ARM Mali](https://en.wikipedia.org/wiki/Mali_(GPU)) line of GPUs, with an
emphasis on the OpenGL ES drivers.

You can read more about my professional career on
[my LinkedIn profile](https://www.linkedin.com/in/erik-faye-lund-7a87112/).

### Talks

I regularly go to tech-conferences and present talks about various graphics
related topics. Here's some of them!

- XDC 2024: [Panfrost Update](https://indico.freedesktop.org/event/6/contributions/305/) - Together with Boris Brezillon
- XDC 2022: [Implementing the Graphics Pipeline on Compute](https://indico.freedesktop.org/event/2/contributions/63/)
- XDC 2022: [Post OpenGL: casual graphics development](https://indico.freedesktop.org/event/2/contributions/70/)
- FOSDEM 2020: [Zink Update: The last year in OpenGL on Vulkan](https://archive.fosdem.org/2020/schedule/event/zink/)
- FOSDEM 2020: [Modernizing mesa3d.org: Let's bring mesa3d.org past web 1.0](https://archive.fosdem.org/2020/schedule/event/mesa3d_website/)
- XDC 2019: [Zink: OpenGL on Vulkan](https://www.youtube.com/watch?v=oU_PwZ1MEYA) - [slides](https://lpc.events/event/5/contributions/329/attachments/433/687/XDC2019-Zink-slide-deck.pdf)
- Siggraph 2019: [Zink: OpenGL on Vulkan](https://www.youtube.com/watch?v=n0Log5goN7Q) - [slides](https://www.khronos.org/assets/uploads/developers/presentations/Vulkan-09-Zink-OpenGL-on-Vulkan-SIGGRAPH-Jul19.pdf) - Part of the Khronos BoF
- Siggraph 2019: [Creating Programming meets Explicit APIs](https://www.youtube.com/watch?v=5atF95TPpVE&t=2245s)
- FOSDEM 2019: [Zink: OpenGL on Vulkan](https://archive.fosdem.org/2019/schedule/event/zink/)
- XDC 2018: [Zink: OpenGL on Vulkan](https://www.youtube.com/watch?v=ukrB-Lbl_Jg) - [slides](https://xdc2018.x.org/slides/zink-lightning-slides.pdf) - Lightning talk
- FOSDEM 2014: [Grate: Liberating NVIDIA's Tegra GPU](https://archive.fosdem.org/2014/schedule/event/grate_driver/)
- Siggraph 2008: [Demoscene Panel](/images/ZINE-Siggraph08.pdf) - Part of a panel discussion about the demoscene, hosted by [Kevin Mack](https://www.kevinmackart.com/)
- Assembly 2006: [Cutting corners, not polygons](https://web.archive.org/web/20060924112836/http://www.assembly.org/2006/seminars/sessions#corners) - No recording or slides available

### Linux Format

I have sometimes had my writings published in [Linux Format](https://www.linuxformat.com/). Here's a few cases:

- Issue 12/2024: [Arm Wrestle](/images/linux-format-2024-12.jpg)
- Issue 09/2023: [Imagining Zink](/images/linux-format-2023-09.jpg)
- Issue 06/2020: [DirectX & Mesa](/images/linux-format-2020-06.jpg)
- Issue 01/2019: [Say Hello to Zink!](/images/linux-format-2019-01.png)

### Demoscene

I'm also actively contributing to
[Demoscene](https://en.wikipedia.org/wiki/Demoscene) productions, in particular
under the label [Excess](http://www.pouet.net/groups.php?which=1360). The other
currently active half of Excess is
[Bent "gloom" Stamnes](https://twitter.com/gloom303).

I've made [a lot](http://www.pouet.net/user.php?who=1383&show=credits) of demos,
and some other notable labels I've released under are:

* [Shitfaced Clowns](http://www.pouet.net/groups.php?which=4843)
* [Carl B](http://www.pouet.net/groups.php?which=2588)
* [Boozoholics](http://www.pouet.net/groups.php?which=2823)

The videos on the top of the home-page showcase some of these demos.

I've also been involved in organizational work in the demoscene, for instance:

* Being an organizer of the creative competitions at
  [The Gathering](https://www.gathering.org/) in 2001 and 2002.
* Being a [Creative Mentor](https://archive.gathering.org/tg12/en/creative/mentors/)
  at The Gathering 2012.
* Organizing [Kindergarden](http://kg.slengpung.com) in 2003 through 2007.
* Taking part in the [Meteoriks](http://meteoriks.org) jury in 2018 and 2019.
* Being an organizer of the competitions at
  [Evoke 2019](https://2019.evoke.eu/) and
  [Black Valley](https://blackvalley.party/).

### Links

You can find the source code of most of my works on
[GitHub](https://github.com/kusma). Here's some highlighted projects, though:

* [Rocket](https://github.com/rocket/rocket): This is a sychronization tool for
  demoscene productions. It's written in C/C++, using [Qt](https://www.qt.io/)
  for the user interface.
* [Grate](https://github.com/grate-driver/grate): This is a reverse-engineered
  driver for the [NVIDIA Tegra 2 GPU](https://en.wikipedia.org/wiki/Tegra#Tegra_2).
  It's developed in cooperation with
  [Thierry "tagr" Reding](https://github.com/thierryreding) and
  [Dmitry "digetx" Osipenko](https://github.com/digetx).
* [Pimpmobile](https://github.com/kusma/pimpmobile): This is a [MOD]/[XM]
  music playback engine for the
  [Nintendo Game Boy Advance](https://en.wikipedia.org/wiki/Game_Boy_Advance).
  It's developed in cooperation with Jørn Nystad.
* [Very Last Engine Ever](https://github.com/excess-demogroup/vlee): This is
  the source code of most of my demos. It's an aging code-base that uses
  DirectX 9 for rendering.
* [Even Laster Engine](https://github.com/excess-demogroup/even-laster-engine):
  This is a rather hacky, but new demo-engine which is the basis for my recent
  demos. It uses Vulkan 1.0 for rendering.

[MOD]: https://en.wikipedia.org/wiki/MOD_(file_format)
[XM]: https://en.wikipedia.org/wiki/XM_(file_format)
