---
title: Home
---
## About me

I'm a graphics programmer, specializing in low-level OpenGL / DirectX, and
other GPU-related development.

[Read more]({{< relref "about.md" >}})

---

{{< latest-post >}}

## Contact

If you need to reach me, please do not hesitate to contact me via e-mail at
[kusmabite@gmail.com](mailto:kusmabite@gmail.com), Mastodon at
[@kusma@floss.social](https://floss.social/@kusma), or Matrix at
[@kusma:matrix.org](https://matrix.to/#/@kusma:matrix.org). I can also
usually be found on IRC as "kusma", hanging around in #tegra on LiberaChat,
#dri-devel on OFTC.
