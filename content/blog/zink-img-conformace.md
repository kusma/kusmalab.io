---
title: "Zink brings conformant OpenGL on Imagination GPUs"
date: 2023-07-06 21:15:00 +0200
tags: [Zink, OpenGL, Vulkan, Mesa, ImgTech]
canonical_url: "https://www.collabora.com/news-and-blog/news-and-events/zink-on-imagination-gpus.html"
---
Today, [Imagination Technologies] announced some very exciting news: they are now [using Zink for full OpenGL 4.6 support]! Collabora had the pleasure of working together with engineers from Imagination to make this a reality, and it's very rewarding to now be able show the results to the world!

<!--more-->

More importantly, this is the first time we've seen a hardware vendor trust the OpenGL-on-Vulkan Mesa driver enough to
completely *side-step* a native OpenGL driver and use it in a *shipping
product*. It's wonderful to see that Zink can realistically be used as a
work-horse, **especially** in a high-performance graphics setting.

Zink [started out] as a small R&D project at Collabora, but has since grown
to be a full-on community project. None of this would have been possible
without the awesome work done by [Mike] and the other Zink contributors!

## Conformance

One small detail from Imagination's post that I think is important to
highlight is that the solution is [officially conformant]. This is the first
product to be officially conformant using Zink, but it's not going to be the
last! In fact, we only need one more conformant implementation before Zink
itself is conformant as a generic layered implementation, according to
the Khronos [Conformant Product Criteria].

## An Open Source graphics future

In the not too distant future, we should be able to combine Zink with the
in-progress [open source driver] from Imagination, and that's when things
will *really* start to shine for the open source graphics stack on
Imagination hardware. So there's plenty more to look forward to here!

[Imagination Technologies]: https://www.imaginationtech.com/
[using Zink for full OpenGL 4.6 support]: https://blog.imaginationtech.com/imagination-gpus-now-support-opengl-4.6
[started out]: https://www.collabora.com/news-and-blog/blog/2018/10/31/introducing-zink-opengl-implementation-vulkan/
[Mike]: https://www.supergoodcode.com/
[officially conformant]: https://www.khronos.org/conformance/adopters/conformant-products/opengl#submission_332
[Conformant Product Criteria]: https://www.khronos.org/files/conformance_procedures.pdf
[open source driver]: https://developer.imaginationtech.com/open-source-gpu-driver/
