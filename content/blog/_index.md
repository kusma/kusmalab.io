---
title: Blog
menus:
  main
url: /blog/
outputs:
  - html
  - rss
---

Welcome to my blog! I'm writing mostly about graphics technology and a bit
about what's going on in my professional life.
